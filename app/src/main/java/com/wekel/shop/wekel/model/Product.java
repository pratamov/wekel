package com.wekel.shop.wekel.model;

import java.util.ArrayList;

public class Product {

    private String name;
    private int price;
    private String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public static ArrayList<Product> generate() {
        ArrayList<Product> products = new ArrayList<Product>();
        Product product1 = new Product();
        product1.setImage("http://5.135.160.204/assets/finhacks/almari.png");
        product1.setName("Lemari");
        product1.setPrice(1500000);
        Product product2 = new Product();
        product2.setImage("http://5.135.160.204/assets/finhacks/ranjang.jpg");
        product2.setName("Ranjang");
        product2.setPrice(5000000);
        Product product3 = new Product();
        product3.setImage("http://5.135.160.204/assets/finhacks/almari2.png");
        product3.setName("Lemari Baju");
        product3.setPrice(2000000);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        return products;
    }
}
